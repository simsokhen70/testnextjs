import React from "react";
import styles from "@/styles/Home.module.css";
import Link from "next/link";
import { usePathname } from "next/navigation";
import ActiveLink from "./ActiveLink";

export default function Navbar() {
  const activeTab = usePathname();
  return (
    <div>
      <header>
        <ul className={styles.navbar}>
          <li>
            <Link href="/"> Home </Link>
          </li>
          <li>
            <Link href="/about"> About </Link>
          </li>
          <li>
            <Link href="/policy"> Policy </Link>
          </li>
        </ul>
      </header>
      <ActiveLink href="/">Home</ActiveLink>
      <ActiveLink href="/about">AboutUs</ActiveLink>
      <ActiveLink href="/policy">Policy</ActiveLink>
    </div>
  );
}
