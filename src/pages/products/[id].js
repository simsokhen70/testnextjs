import Navbar from "@/components/Navbar";
import { useRouter } from "next/router";
import React from "react";

export default function ProductDetail() {
  const router = useRouter();
  console.log(router.query, "router.query")
  return (
    <div>
      <Navbar />
      <h1>Request {router.query.id}</h1>
    </div>
  );
}
